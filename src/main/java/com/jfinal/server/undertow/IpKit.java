package com.jfinal.server.undertow;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class IpKit {
    
    public static List<String> getLocalIp() {
        List<String> ipList = new ArrayList<>();
        try {
            for (Enumeration<NetworkInterface> e = NetworkInterface.getNetworkInterfaces(); e.hasMoreElements();) {
                NetworkInterface networkInterface = e.nextElement();
                if (networkInterface.isLoopback() || networkInterface.isVirtual() || !networkInterface.isUp()) {
                    continue;
                }
                
                for (Enumeration<InetAddress> ele = networkInterface.getInetAddresses(); ele.hasMoreElements();) {
                    InetAddress ip = ele.nextElement();
                    if (ip instanceof Inet4Address) {
                        ipList.add(ip.getHostAddress());
                    }
                }
            }
            // return InetAddress.getLocalHost().getHostAddress();
            return ipList;
        } catch (Exception e) {
            // return "127.0.0.1";
            return ipList;
        }
    }
}




